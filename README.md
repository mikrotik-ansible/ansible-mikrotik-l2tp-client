# Mikrotik L2TP client configurator.

A Role allows configuraiton of Mikrotik RouterOS L2TP client interface with Ansible.

The role creates PPP profiles and L2TP client interfaces.

Examples of usage with comments are in docs/

Big thanks to Martin Dulin for his role https://github.com/mikrotik-ansible/mikrotik-firewall.
His role gave me an idea how solve RouterOS configuration tasks.
