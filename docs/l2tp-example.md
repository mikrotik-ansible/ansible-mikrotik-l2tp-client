---
Example configuration.
Use it as a reference.

Be careful: the role only sets defined variables.
If you set a variable, apply configuration and then
you undefine the variable - no any config changes will be done.
The value will remain the same as the last defined variable.

```
mikrotik_l2tp_client_delete_old_profiles: <yes|no> # if 'yes' - delete ALL existing PPP profiles. Deletes even profiles which are not used by L2TP but used by other PPP connections.
mikrotik_l2tp_client_delete_old_connections: <yes|no> # if 'yes' - delete ALL existing L2TP client connections
mikrotik_l2tp_client:
  profiles:
    - name: profile 1 # Required. name of new profile.
      address_list: list1 # name of an address list to add address of interface to
      comment: comment 1 # Required. comment to add to a profile
      idle_timeout: 3600 # Specifies the amount of time after which the link will be terminated if there are no activity present. Timeout is not set by default
      interface_list: int_list 1 #name of an interface list to add the client to
      on_up: script1 # name of a script to start on interface up
      remote_address: 10.8.0.1 # Assigns an individual address to the PPP. Tunnel address or name of the pool from which address is assigned to remote ppp interface.
      use_encryption: <yes | no | default | require> # Set encryption. Must be quoted
      wins_server: 10.8.0.1 # Windows Internet Naming Service server
      bridge: bridge1 # Name of the bridge interface to which ppp interface will be added as slave port. Both tunnel end point (server and client) must be in bridge in order to make this work.
      #bridge_port_priority:  # No documentation on wiki.mikrotik.com.
      copy_from: 0 # Item number to copy settings from. Not recommended for usage because of unpredictability of numbering
      incoming_filter: chain_inc_1 #Firewall chain name for incoming packets
      local_address: 10.8.0.10 # Tunnel address or name of the pool from which address is assigned to ppp interface locally.
      only_one: <yes|no> # Allow only one connection at a time
      #queue_type: #  No documentation on wiki.mikrotik.com.
      session_timeout: 1d # The maximum time the connection can stay up
      use_mpls: <yes | no | default | require> # Must be quoted. Specifies whether to allow MPLS over L2TP
      #bridge_horizon: #  No documentation on wiki.mikrotik.com.
      change_tcp_mss: <yes|no> # Change or not TCP protocol's Maximum Segment Size
      dns_server: 10.0.0.11 # IP address of the DNS server that is supplied to ppp clients
      #insert_queue_before: #  No documentation on wiki.mikrotik.com.
      on_down: script_down_1 # script to run on interface down
      outgoing_filter: chain_out_1 # Firewall chain name for outgoing packets
      # Rate limitation in form of rx-rate[/tx-rate] [rx-burst-rate[/tx-burst-rate] [rx-burst-threshold[/tx-burst-threshold]
      # [rx-burst-time[/tx-burst-time] [priority] [rx-rate-min[/tx-rate-min]]]]
      # from the point of view of the router (so "rx" is client upload, and "tx" is client download).
      # All rates are measured in bits per second, unless followed by optional 'k' suffix (kilobits per second)
      # or 'M' suffix (megabits per second).
      # If tx-rate is not specified, rx-rate serves as tx-rate too.
      # The same applies for tx-burst-rate, tx-burst-threshold and tx-burst-time.
      # If both rx-burst-threshold and tx-burst-threshold are not specified
      # (but burst-rate is specified), rx-rate and tx-rate are used as burst thresholds.
      # If both rx-burst-time and tx-burst-time are not specified, 1s is used as default.
      # Priority takes values 1..8, where 1 implies the highest priority, but 8 - the lowest.
      # If rx-rate-min and tx-rate-min are not specified rx-rate and tx-rate values are used.
      # The rx-rate-min and tx-rate-min values can not exceed rx-rate and tx-rate values.
      rate_limit: 1000M
      use_compression: <yes|no> # Specifies whether to use data compression or not.
      #use_upnp: #  No documentation on wiki.mikrotik.com.

  connections:
    - name: l2tp-out-1 # required argument
      user: user_1 # Required. L2TP auth user
      connect_to: 10.7.0.1 # Required. The IP address of the L2TP server to connect to
      comment: my connection # Required. Comment to add to connection
      add_default_route: <yes|no> # Whether to use server which this client is connected to as its default gateway
      #allow_fast_path: <yes|no> #  No documentation on wiki.mikrotik.com. Could be the setting to enable FastPath processing
      copy_from:  0 # Item number to copy settings from. Not recommended for usage because of unpredictability of numbering
      dial_on_demand: <yes|no> # Enable/disable dial on demand
      use_ipsec: <yes|no> # Enables IPsec encryption with default settings in PSK mode.
      ipsec_secret: password # Secret to use with "use_ipsec" in PSK mode. Be careful to use only RouterOS supported symbols.
      max_mru: 1500 # Maximum Receive Unit. Max packet size that PPP interface will be able to receive without packet fragmentation.
      mrru: <disabled | integer> # Maximum packet size that can be received on the link. If a packet is bigger than tunnel MTU, it will be split into multiple packets, allowing full size IP or Ethernet packets to be sent over the tunnel.
      password: password # L2TP auth password. Be careful to use only RouterOS supported symbols.
      allow: pap,chap,mschap1,mschap2 # must be quoted. The authentication method to allow for the client
      default_route_distance: 10 # Distance to set to created default route if "add_default_route" is enabled
      disabled: <yes|no> # Required. if the connection is disabled
      keepalive_timeout: 10 # PPP keepalive timeout in seconds.
      max_mtu: 1500 # Maximum Transmission Unit. Max packet size that PPP interface will be able to send without packet fragmentation.
      profile: "profile 1" # name of PPP profile to use for the connection.
```
